/** this script is redirecting the client to the chosen search engine, if he is pressing the enter button in the search bar or clicks the search button **/

function search() {
    let url;
    let searchstring = document.getElementById("searchstring").value;
    if (searchstring === '') {
        alert("Bitte einen Suchbegriff eingeben!")
    } else {
        switch (document.getElementById("searchengine").value) {
            case "DuckDuckGo":
                url = "https://duckduckgo.com/?q=" + searchstring;
                break;
            case "Google":
                url = "https://www.google.de/search?q=" + searchstring;
                break;
            case "Metager":
                url = "https://metager.de/meta/meta.ger3?eingabe=" + searchstring + "&submit-query=&focus=web";
                break;
            default:
                alert("Ungültige Suchmaschine ausgewählt!");
        }

        window.location.href = url;
    }
}

document.getElementById("searchsubmit").addEventListener("click", e => {
    search();
});

document.querySelector("#searchstring").addEventListener("keypress", e => {
    if (e.key === "Enter") {
        search();
    }
});
