/** fetch the json file with the content and put it into HTML code, that will be written to the main page **/
fetch("data/content.json")
    .then(response => response.json())
    .then(json => {
        let output = "";

        for (let category in json) {
            let sites = json[category];

            output += "<div class=\"col mb-4\"><ul class=\"list-group\"><li aria-current=\"true\" class=\"list-group-item active fw-bolder\">" + category + "</li>";

            for (let site in sites) {
                output += "<li class=\"list-group-item\"><a href=\"" + sites[site] + "\">" + site + "</a></li>";
            }

            output += "</ul></div>";
        }

        document.getElementById("boxlist").innerHTML = output;
    });
