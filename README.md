# hc4-intranet
Dies ist eine leichtgewichtige HTML5 Landing-Page auf Bootstrap 5 Basis.  
Sie wurde entwickelt, um auf einem Odroid-HC4 unter dem lighttpd eines PiHole ausgeliefert zu werden. Afaik ist jedoch nur ein beliebiger, einfacher Webserver auf einem beliebigen System notwendig, welcher die Dateien ausliefert.

Die Landing-Page hat dank Bootstrap ein responsive Design.

## Setup
Einfach das Projekt in den Document Root des Webservers kopieren, die Datei `data/content.sample.json` nach `data/content.json` kopieren und dessen Inhalt anpassen. Ggf. müssen die Rechte vom Verzeichnis angepasst werden.

## Hintergrundbild
Das Hintergrundbild stammt von HarryJBurgess auf Pixabay. [Link](https://pixabay.com/de/photos/tromso-norwegen-skandinavien-reisen-4991531/)
Durch das Ersetzen der Datei `background.jpg` kann das Hintergrundbild optional ersetzt werden.

## Lizensierung
- Hintergrundbild: https://pixabay.com/de/service/license/
- Bootstrap: https://github.com/twbs/bootstrap/blob/main/LICENSE
- dieses Projekt: siehe LICENSE Datei
